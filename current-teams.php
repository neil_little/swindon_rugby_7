<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/current-teams">
	<meta name="description" content="">
	<title>Swindon 7's: Current Teams</title>

	<meta property="og:url" content="http://swindon7s.co.uk/current-teams">
	<meta property="og:title" content="Swidon 7's: Current Teams"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="current-teams small-12 columns">

			<h1 class="heading">Current Teams</h1>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>