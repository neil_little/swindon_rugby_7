<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/need-a-logo">
	<meta name="description" content="">
	<title>Swidon 7's: Need a logo</title>

	<meta property="og:url" content="http://swindon7s.co.uk/need-a-logo">
	<meta property="og:title" content="Swidon 7's: Need a logo"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<div id="promoCanvas" class="logo-beasts small-break-up small-break-down">
				<section class="title-container small-5 columns">
					<h1 class="no-select"><span class="blue-fnt">Need</span> a team logo</h1>
				</section>
				<section class="small-7 columns">
					<a href="mailto:matt@bravedog.co.uk?subject=INTREST IN LOGO" class="button big-ol need-logo radius">Get Me a logo</a>
				</section>
			</div>

			<section class="join-strip no-select small-12 colums">
				<?php require_once("parts/join-strip.php"); ?>
			</section>

			<?php require_once("parts/3-block-adv.php"); ?>
		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>