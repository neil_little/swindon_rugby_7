<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/news">
	<meta name="description" content="">
	<title>Swindon 7's: News</title>

	<meta property="og:url" content="http://swindon7s.co.uk/news">
	<meta property="og:title" content="Swidon 7's: News"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="news small-12 columns">

			<style>
				li.f-twitter {
					/*display: none !important;*/
				}
			</style>

			<section id="social-stream"></section>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>