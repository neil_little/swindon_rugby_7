<!doctype html>
<!--[if lte IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/festival">
	<meta name="description" content="">
	<title>Swindon 7's: Festival</title>

	<meta property="og:url" content="http://swindon7s.co.uk/festival">
	<meta property="og:title" content="Swidon 7's: Festival"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<section class="festival-list">
				<ul class="links small-4 columns">
					<li><a href="#about_us">About Us</a></li>
					<!-- <li><a href="#photos">Photos</a></li> -->
					<li><a href="#video">Video</a></li>
					<li><a href="#getting_there">Getting Here</a></li>
					<li><a href="#traders">Traders</a></li>
					<li><a href="#volunteers">Volunteers</a></li>
					<li><a href="#press">Press</a></li>
				</ul>

				<ul class="festival-breakdown small-8 columns">
					<li class="festival-item" id="about_us">

						<img class="force-bottom" src="/images/cam-brown-pass.jpg" alt="Cam Brown">

						<p>Swindon 7s Festival is the sport and music festival that takes place every year on 19.07.14. Launched in 20012 the Festival has quickly become renowned as the Sporting Vfest, attracting hundreds of sports enthusiasts and festival goers. The build up to 2014 is in full swing and will see some exciting additions to an already impressive line up of enthralling sport, music and entertainment.</p>

						<p>We are proud to have created a unique event that has made a significant impact on the world's sporting scene, with many different teams competing every year.</p>
					</li>

					<!-- <li class="festival-item" id="photos">

						<p>NEED TO FILL</p>
					</li> -->

					<li class="festival-item" id="video">

						<iframe width="700" height="394" src="//www.youtube-nocookie.com/embed/I--6AexCUZ8" frameborder="0" allowfullscreen></iframe>
					</li>

					<li class="festival-item" id="getting_there">

						<strong>Venue</strong>

						<p>Swindon 7s Festival takes place annually at the Swindon Rugby Club.</p>

						<p>The club is easily accessible by road and lies next to Green Bridge and WHSmiths.</p>

						<ul class="special-border">
							<li>Swindon Rugby Club</li>
							<li>New Pavilion</li>
							<li>Greenbridge Road</li>
							<li>Swindon</li>
							<li>Wiltshire</li>
							<li>SN3 3LA</li>
							<li>Tel: 01793 521148</li>
						</ul>

						<p>Parking is limited so we advise that people car share or use public transport where possible.</p>

						<strong>Car Park Opening Times</strong>
						
						<ul class="special-border">
							<li>Saturday: 7:30am - 8pm</li>
						</ul>

						<strong>Taxi Service</strong>

						<p>Swindon 7s Festival has a number of Taxi partners from across the area. Taxis will run to and from a dedicated taxi rank within the festival site.</p>
					</li>

					<li class="festival-item" id="traders">
						<p>At the sport and music festival, trading at Swindon 7s Festival gives you a unique opportunity to interact with over hundreds of festival goers across a varied demographic profile.</p>

						<p>Traders will receive a dedicated plot in the Main Festival Arena dependent on their targeted audience and requirements. Plots will be allocated on a first come first served basis.</p>

						<p>If you would like to apply to bring a concession to trade at Swindon 7s 2014 please complete our simple.</p>

						<p>
							<a class="button big-ol radius" href="mailto:info@swindon7s.co.uk?subject=Swindon 7s Trader - How Can I Be A Trader?">APPLY</a>
						</p>
					</li>

					<li class="festival-item" id="volunteers">
						<p>
							<strong>WHAT IT TAKES TO BE A SWINDON7s VOLUNTEER</strong>
						</p>

						<ul class="special-border">
							<li>Be 18 or over</li>
							<li>Able to commit to volunteer a minimum of 10 hours</li>
							<li>Available to attend a volunteer briefing in before the festival 2014</li>
							<li>Be eligible to volunteer in the UK</li>
						</ul>

						<p>
							<a class="button big-ol radius" href="mailto:info@swindon7s.co.uk?subject=Swindon 7s Volunteer - More Information">APPLY NOW</a>
						</p>
					</li>

					<li class="festival-item" id="press">
						<p>Welcome to the Swindon 7s Festival Press Page. To make covering the festival as easy as possible we have included links to everything we think you'll need.</p>

						<p><a href="/docs/swindon-7s-press-release.pdf">Click here</a> to download the Full 2014 Press Release</p>

						<p><a href="/docs/swindon-7s-brand-guidelines.zip">Click here</a> to download a zip file containing Swindon 7s Festival logos and brand guidelines</p>

						<p>Should you require any additional information or artwork please email <a href="mailto:info@Swindon7s.co.uk">Info@Swindon7s.co.uk</a>.</p>
					</li>
				</ul>
			</section>

			<?php require_once("parts/3-block-adv.php"); ?>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>