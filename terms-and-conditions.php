<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/">
	<meta name="description" content="Want to know the nitty gritty, here it is. Have a good read.">
	<title>Swindon 7's: Terms & Conditions</title>

	<meta property="og:url" content="http://swindon7s.co.uk/terms-and-conditions">
	<meta property="og:title" content="Swidon 7's: Terms & Conditions"/>
	<meta property="og:description" content="Want to know the nitty gritty, here it is. Have a good read."/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="terms-conditions small-12 columns">

			<h1>Terms & Conditions</h1>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, dicta nemo dolor tempora nihil cumque ex assumenda corrupti optio temporibus laudantium praesentium molestiae inventore itaque pariatur accusamus vitae soluta quos!</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, molestiae, ex, explicabo reprehenderit provident vel adipisci consectetur in accusantium consequuntur rem quas labore nesciunt iste dicta illum tempora fuga laboriosam.</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, deserunt, magnam, itaque autem eaque at delectus accusamus illum blanditiis alias minima vel dolores officiis similique earum. Reiciendis, repellat veritatis hic et. Quae, quos, excepturi, ad, iste soluta repellat temporibus suscipit fuga facilis quod reiciendis quisquam sapiente deleniti distinctio laudantium consectetur tempore aperiam at praesentium numquam doloremque sequi natus blanditiis voluptatibus dolorem. Veniam, architecto sit voluptatum quas alias odio sint nobis voluptatibus aperiam ratione. Eveniet, rerum eaque aliquam ipsam necessitatibus cum sequi vitae numquam. Voluptatem, excepturi, nam, laudantium ut quod quae doloremque tempora commodi modi earum deleniti incidunt eveniet labore veniam nesciunt aperiam voluptates. Magni, ab, consequuntur. Aut, ea sit magni quidem labore culpa quam dignissimos veritatis odit ratione. Odio, minima, corporis, numquam quis quas esse harum cupiditate nemo ipsa non nulla aperiam molestias officia. Rem, fugit, quia, cum quasi quas illo vitae impedit libero adipisci cumque provident sed nemo nulla natus minus quaerat ut. Velit, repellat, saepe, ipsam, hic deleniti porro assumenda molestiae neque magni totam deserunt quas soluta similique praesentium minima asperiores corporis est quod ut unde reprehenderit et doloribus. At, aperiam eius delectus sequi nulla explicabo obcaecati? Commodi, non laboriosam nulla accusamus totam quaerat veniam hic blanditiis et.</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, numquam, nostrum, illum, incidunt delectus quia eveniet suscipit maiores temporibus repudiandae dignissimos in. Animi, quis, cumque est impedit suscipit perferendis magni adipisci perspiciatis consequatur deserunt incidunt tempora sequi quibusdam distinctio illo natus ipsam harum et necessitatibus fugiat veritatis iure ab vel error eum aliquid sint quia quos tenetur nemo alias expedita.</p>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>