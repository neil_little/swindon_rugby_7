<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/payment-complete">
	<meta name="description" content="">
	<title>Swindon 7's: Payment Complete</title>

	<meta property="og:url" content="http://swindon7s.co.uk/payment-complete">
	<meta property="og:title" content="Swidon 7's: Payment Complete"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="payment-complete small-12 columns" style="min-height:580px">

			<h1>Thanks for the <span class="blue-fnt">payment</span></h1>

			<h2 class="explain">Your details from <span class="blue-fnt">earlier</span> have been sent to us!<br>If you have any questions please use the <a class="blue-fnt" href="/contact-us">contact form</a>!</h2>

			<h4 class="access-url">
				<a href="/">Take Me Home</a>
			</h3>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>