require 'compass/import-once/activate'
# Require any additional compass plugins here.
add_import_path "bower_components/foundation/scss"

# Set this to the root of your project when deployed:
#http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "build"
fonts_dir = "fonts"

output_style = :compressed
environment = :development

relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true
color_output = true

preferred_syntax = :scss
