<!doctype html>
<html lang="en" class="no-js">
<head>
		<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/">
	<meta name="description" content="">
	<title>Swindon 7's: Contact Us</title>

	<meta property="og:url" content="http://swindon7s.co.uk/">
	<meta property="og:title" content="Swidon 7's: Contact Us"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<div id="promoCanvas" class="contact-header">
				<section class="title-container">
					<h1 class="no-select">contact the s7's <span class="blue-fnt">team</span></h1>
				</section>

				<aside class="social-media">
					<a class="target-blank promoCanvas-twitter" href="//twitter.com/Swindon7s"></a>
					<a class="target-blank promoCanvas-facebook" href="//facebook.com/Swindon7sFestival"></a>
				</aside>
			</div>

			<div id="gmap" class="small-12 columns">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2480.047758386223!2d-1.7486540000000002!3d51.567358!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb7caaa681c2189a0!2sSwindon+Rugby+Football+Club!5e0!3m2!1sen!2suk!4v1396054030920"></iframe>
			</div>

			<section class="contact-info">
				<div class="address-breakdown small-6 columns">
					<ul>
						<li>New Pavilion</li>
						<li>Greenbridge Road</li>
						<li>Swindon</li>
						<li>Wiltshire</li>
						<li>SN3 3LA</li>
						<li><a href="tel:+441793521148"><span class="blue-fnt">Tel:</span> 01793 521148</a></li>
						<li><a href="mailto:Info@swindon7s.com"><span class="blue-fnt">Email:</span> Info@swindon7s.com</a></li>
					</ul>
				</div>

				<div class="contact-form small-6 columns">
					<div id="message"></div>

					<form method="post" action="/parts/contact.php" name="contactform" id="contactform" class="contactpage">
						<input class="half force-me" type="text" name="fullname" value="" placeholder="Name">
						<input class="half" type="tel" name="phone" value="" placeholder="Number">
						<textarea class="full" name="message" placeholder="Mssg"></textarea>
						<input class="half force-me" type="email" name="email" value="" placeholder="Email">
						<input type="submit" id="submit" class="button half" value="SUBMIT">
					</form>
				</div>
			</section>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<script src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>