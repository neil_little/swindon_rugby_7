<!doctype html>
<html lang="en" class="no-js">
<head>
		<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/">
	<meta name="description" content="">
	<title>Swindon 7's: Enter A Team</title>

	<meta property="og:url" content="http://swindon7s.co.uk/">
	<meta property="og:title" content="Swidon 7's: Enter A Team"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<div id="promoCanvas" class="med-break-up med-break-down">
				<section class="title-container">
					<h1 class="no-select"><span class="blue-fnt">Our</span> sponsors and partner</h1>
				</section>
				<aside class="social-media">
					<a class="promoCanvas-twitter" href="//twitter.com/Swindon7s" target="_blank"></a>
					<a class="promoCanvas-facebook" href="//facebook.com/Swindon7sFestival" target="_blank"></a>
				</aside>
			</div>

			<article class="sponsor-table">
				<section class="medium-12 large-4 columns">
					<div class="top-blk our-sponsors no-select">
						<h2 class="title">our sponsors</h2>
					</div>

					<?php /* OLD 
					<a href="//pitchero.com/clubs/swindonrfc/">
						<div class="child-blk our-sponsors">
							<img src="/images/sponsor-swindon-srfc.png" alt="??" class="sponsor-logo">
						</div>
					</a>
					*/ ?>

					<a href="//www.silverbacksport.co.uk/">
						<div class="child-blk our-sponsors" style="background-color: rgb(35,31,32);border-left: 0">
							<img src="/images/sponsor-sliverback.png" alt="Silverback" class="sponsor-logo">
						</div>
					</a>
				</section>

				<section class="medium-12 large-4 columns">
		<?php // Temp Button - Untill more image work is received from admin ?>
					<a href="mailto:info@swindon7s.co.uk?subject=Become A Sponsor">
					<div class="top-blk become-a-sponsor no-select">
						<h2 class="title">become a sponsor</h2>
					</div>
		<?php // TEMP END ?>
					</a>

					<a href="//www.greeneking.co.uk/">
						<div class="child-blk partner">
							<img src="/images/sponsor-greene-king.png" alt="Greene King" class="sponsor-logo">
						</div>
					</a>
				</section>

				<section class="medium-12 large-4 columns">
					<div class="top-blk partner no-select">
						<h2 class="title">partners</h2>
					</div>

					<a href="//www.thekhyber.co.uk/">
						<div class="child-blk become-a-sponsor">
							<img src="/images/sponsor-kyber.png" alt="Kyber Indian Food" class="sponsor-logo">
						</div>
					</a>
				</section>
			</article>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>