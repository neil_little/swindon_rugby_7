<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/enter-a-team">
	<meta name="description" content="">
	<title>Swindon 7's: Enter A Team</title>

	<meta property="og:url" content="http://swindon7s.co.uk/enter-a-team">
	<meta property="og:title" content="Swidon 7's: Enter A Team"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<?php require_once("parts/hero-slider.php"); ?>

			<section class="bottom-panel small-break-down">
				<div class="no-select small-5 column">
					<h1 class="massive-text"><span class="blue-fnt">enter</span> a team today</h1>
				</div>
				<div class="small-7 column">
					<a href="/register" class="button big-ol radius">Register</a>
				</div>
			</section>

			<section class="join-strip no-select small-12 colums">
				<?php require_once("parts/join-strip.php"); ?>
			</section>
		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>