<aside class="news-update aside-focus">
	<h2>Also in the News</h2>

	<ul class="news-wrapper">
		<li>
			<img src="/images/news/sidebar-1.jpg" alt="Sidebar-Image-1">
			<a href="/open-door-swings-back-into-action-after-major-facelift.php">
				<p class="news-title">Open Door swings back into action after major facelift</p>
			</a>
		</li>
		<li>
			<img src="/images/news/sidebar-2.jpg" alt="Sidebar-Image-2">
			<a href="/william-proves-a-bright-spark-as-an-apprentice.php">
				<p class="news-title">William proves a bright spark as an apprentice</p>
			</a>
		</li>
	</ul>
</aside>