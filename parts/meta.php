<!-- Meta's -->
<meta charset="UTF-8">
<meta name="viewport" content="width=1200">
<!-- CSS -->
<link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Titillium+Web:400,900,700'>
<link rel="stylesheet" href="css/app.css" />
<link rel="stylesheet" href="css/rfc.css" />
<!-- FAVICONS -->
<link rel="apple-touch-icon-precomposed" href="/images/favicon/apple-touch-icon-precomposed.png">
<link rel="icon" type="image/png" href="/images/favicon/favicon.png">
<!--[if lt IE 9]>
  <script src="/build/html5shiv.min.js"></script>
  <script src="/build/respond.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<![endif]-->
<!-- JS Libary (SCRIPTS @ BOTTOM) -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/modernizr/modernizr.min.js"></script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-47579383-1', 'swindon7s.co.uk');
	ga('send', 'pageview');

</script>