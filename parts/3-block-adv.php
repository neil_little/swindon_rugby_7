<div class="bottom-adv">
	<a href="/enter-a-team">
		<article id="enterTeam" class="advert yellow-blue small-4 columns">
				<h3><span class="blue-fnt">enter</span> a team today!</h3>
		</article>
	</a>
	<a href="/sponsor">
		<article id="becomeSponsor" class="advert blue-white small-4 columns">
				<h3>become <span class="white-fnt">a sponsor or partner</span></h3>
		</article>
	</a>
	<a id="NeedALogo" href="/need-a-logo">
		<article id="logo" class="advert yellow-blue small-4 columns">
			<h3>need <span class="blue-fnt">a team</span>logo?</h3>
		</article>
	</a>
</div>