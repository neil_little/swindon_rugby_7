<nav id="promoSlider">
	<ul id="heroSlider" class="clearfix" 
	data-cycle-slides="li"
	data-cycle-timeout="4000"
	data-cycle-pager=".hero-pager"
	data-cycle-pause-on-hover="true"
	data-cycle-swipe=true>

		<li class="slide-2 medium-4 columns" style="background-image:url(/images/placeholder/slide-1.jpg);">					
			<section class="slider-content">
				<h2 class="slider-heading"><span class="blue-fnt">£1000</span> prize money!</h2>

				<a href="/register" class="button rfc radius">Register</a>
			</section>
		</li>

		<li class="slide-3 medium-4 columns" style="background-image:url(/images/placeholder/slide-2.jpg);">
			<section class="slider-content">
				<h2 class="slider-heading"><span class="blue-fnt">enter</span> a team today!</h2>
				
				<a href="/register" class="button rfc radius">Register</a>
			</section>
		</li>

		<li class="slide-1 medium-4 columns" style="background-image:url(/images/placeholder/slide-3.jpg);">
			<section class="slider-content">
				<h2 class="slider-heading"><span class="blue-fnt">register</span> while you can!</h2>

				<a href="/register" class="button rfc radius">Register</a>
			</section>
		</li>

		<li class="slide-3 medium-4 columns" style="background-image:url(/images/placeholder/slide-silverback.jpg);"></li>
	</ul>

	<div class="hero-pager"></div>

	<div class="promo-date small-1 columns">
		<h1 class="rotated-date">18th & 19th july 2015</h1>
	</div>
</nav>