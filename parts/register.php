<?php if ( !isset( $_SESSION ) ) session_start();

if ( !$_POST ) exit;

if ( !defined( "PHP_EOL" ) ) define( "PHP_EOL", "\r\n" );

///////////////////////////////////////////////////////////////////////////

// Simple Configuration Options

// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

$address = "info@swindon7s.co.uk";

// Twitter Direct Message notification control - (0 = off, 1 = on).
$twitter_active = 0;

$twitter_user    = "";
$consumer_key    = "";
$consumer_secret = "";

$token           = "";
$secret          = "";

// END OF Simple Configuration Options

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// Do not edit the following lines
//
///////////////////////////////////////////////////////////////////////////

$postValues = array();
foreach ( $_POST as $regTeamName => $value ) {
	$postValues[$regTeamName] = trim( $value );
}
extract( $postValues );


// Important Variables
$posted_verify = isset( $postValues['verify'] ) ? md5( $postValues['verify'] ) : '';
$session_verify = !empty($_SESSION['jigowatt']['ajax-extended-form']['verify']) ? $_SESSION['jigowatt']['ajax-extended-form']['verify'] : '';

$error = '';

///////////////////////////////////////////////////////////////////////////
//
// Begin verification process
//
// You may add or edit lines in here.
//
// To make a field not required, simply delete the entire if statement for that field.
//
///////////////////////////////////////////////////////////////////////////


// CURRENT DATE field is required
if ( empty( $regTeamName ) ) {
	$error .= '<li>A Team Name is required.</li>';
}

// Email field is required
if ( empty( $email ) ) {
	$error .= '<li>Your PayPal e-mail address is required.</li>';
} elseif ( !isEmail( $email ) ) {
	$error .= '<li>You have entered an invalid e-mail address.</li>';
}

// MAIN CONTACT DETAILS field is required
if ( empty( $mainContact ) ) {
	$error .= '<li>We need main contact details to carry on.</li>';
}

// TEAM PLAYERS - field is required
if ( empty($playerName1) ) { $error .= '<li>Enter box-1 field.</li>'; }
if ( empty($playerName2) ) { $error .= '<li>Enter box-2 field.</li>'; }
if ( empty($playerName3) ) { $error .= '<li>Enter box-3 field.</li>'; }
if ( empty($playerName4) ) { $error .= '<li>Enter box-4 field.</li>'; }
if ( empty($playerName5) ) { $error .= '<li>Enter box-5 field.</li>'; }
if ( empty($playerName6) ) { $error .= '<li>Enter box-6 field.</li>'; }
if ( empty($playerName7) ) { $error .= '<li>Enter box-7 field.</li>'; }
if ( empty($playerName8) ) { $error .= '<li>Enter box-8 field.</li>'; }
if ( empty($playerName9) ) { $error .= '<li>Enter box-9 field.</li>'; }
if ( empty($playerName10) ) { $error .= '<li>Enter box-10 field.</li>'; }
if ( empty($playerName11) ) { $error .= '<li>Enter box-11 field.</li>'; }
if ( empty($playerName12) ) { $error .= '<li>Enter box-12 field.</li>'; }
if ( empty($playerName13) ) { $error .= '<li>Enter box-13 field.</li>'; }
if ( empty($playerName14) ) { $error .= '<li>Enter box-14 field.</li>'; }
if ( empty($playerName15) ) { $error .= '<li>Enter box-15 field.</li>'; }
if ( empty($playerName16) ) { $error .= '<li>Enter box-16 field.</li>'; }


// MAIN CONTACT DETAILS - field is required
if ( empty( $teamContactInfo ) ) {
	$error .= '<li>Team contact info must be entered to send.</li>';
}

if ( !empty($error) ) {
	echo '<div class="error_message">';
	echo '<h1><strong class="red-fnt">Attention!</strong> Please correct the errors below and try again.</h1>';
	echo '<ul class="error_messages">' . $error . '</ul>';
	echo '</div>';

	// Important to have return false in here.
	return false;

}

// Advanced Configuration Option.

$e_subject = "Swindon 7s: $regTeamName - Registration Part 1 Of 2";

$msg  = "---------" . PHP_EOL . PHP_EOL;

$msg .= "Important: Following this email (providing you have completed the online payment process) you will receive payment confirmation from PayPal.  If you do not receive a PayPal confirmation your team may not have been registered, in which case please call us on: 01793 521148" . PHP_EOL . PHP_EOL;

$msg .= "---------" . PHP_EOL . PHP_EOL;


$msg .= "Please ensure the team details you have supplied are correct:" . PHP_EOL . PHP_EOL;


$msg .= "Team name: $regTeamName" . PHP_EOL . PHP_EOL;

$msg .= "Registration Email: $email" . PHP_EOL . PHP_EOL;

$msg .= "Main Contact Details:" . PHP_EOL . PHP_EOL;
$msg .= "$mainContact" . PHP_EOL . PHP_EOL . PHP_EOL;

$msg .= "Team Names:" . PHP_EOL . PHP_EOL;

$msg .= "1 - $playerName1" . PHP_EOL;
$msg .= "2 - $playerName2" . PHP_EOL;
$msg .= "3 - $playerName3" . PHP_EOL;
$msg .= "4 - $playerName4" . PHP_EOL;
$msg .= "5 - $playerName5" . PHP_EOL;
$msg .= "6 - $playerName6" . PHP_EOL;
$msg .= "7 - $playerName7" . PHP_EOL;
$msg .= "8 - $playerName8" . PHP_EOL;
$msg .= "9 - $playerName9" . PHP_EOL;
$msg .= "10 - $playerName10" . PHP_EOL;
$msg .= "11 - $playerName11" . PHP_EOL;
$msg .= "12 - $playerName12" . PHP_EOL;
$msg .= "13 - $playerName13" . PHP_EOL;
$msg .= "14 - $playerName14" . PHP_EOL;
$msg .= "15 - $playerName15" . PHP_EOL;
$msg .= "16 - $playerName16" . PHP_EOL . PHP_EOL . PHP_EOL;

$msg .= "Further Information:" . PHP_EOL . PHP_EOL;
$msg .= "$teamContactInfo" . PHP_EOL . PHP_EOL;

$msg .= "--------------------------------------------------------------------------------------" . PHP_EOL;
$msg .= "This message was sent to you via Swindon 7's Contact Form made by Bravedog";

if ( $twitter_active == 1 ) {

	$twitter_msg = $regTeamName . " - " . $message . ". You can contact " . $regTeamName . " via email, " . $email ." or via phone " . $phone . ".";
	twittermessage( $twitter_user, $twitter_msg, $consumer_key, $consumer_secret, $token, $secret );

}

$msg = wordwrap( $msg, 100 );

$headers  = "From: $email" . PHP_EOL;
$headers .= "Reply-To: $address" . PHP_EOL;
$headers .= "Bcc: $address" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

if ( mail( $email, $e_subject, $msg, $headers ) ) {

	echo "<div id='success_page'>";
	echo "<h1>Email Sent <span>Successfully.</span></h1>";
	echo "<p>Thank you <strong>$regTeamName</strong>, your message has been submitted to us.</p>";
	

	echo '<form action="https://www.paypal.com/cgi-bin/webscr" method="post">';
	echo '<h2>Registration Payment</h2>';
	echo '<p>Please fill in the form below to pay the access the registeration fee.</p>';
	echo '<input type="hidden" name="cmd" value="_xclick">';
	echo '<input type="hidden" name="business" value="swindon7s@gmail.com">';
	echo '<input type="hidden" name="item_name" value="Swindon 7s Registration Fee">';
	echo '<input type="hidden" name="amount" value="75.00">';
	echo '<input type="hidden" name="no_shipping" value="0">';
	echo '<input type="hidden" name="no_note" value="1">';
	echo '<input type="hidden" name="currency_code" value="GBP">';
	echo '<input type="hidden" name="lc" value="AU">';
	echo '<input type="hidden" name="bn" value="PP-BuyNowBF">';
	echo '<input type="hidden" name="return" value="http://swindon7s.co.uk/payment-complete.php">';
	echo '<input type="submit" value="Pay Registration" id="paypalBtn" class="button">';
	echo '</form>';

	echo "</div>";

	return false;

}


///////////////////////////////////////////////////////////////////////////
//
// Do not edit below this line
//
///////////////////////////////////////////////////////////////////////////
echo 'ERROR! Please confirm PHP mail() is enabled.';
return false;

function twittermessage( $user, $message, $consumer_key, $consumer_secret, $token, $secret ) { // Twitter Direct Message function, do not edit.

	require_once 'twitter/EpiCurl.php';
	require_once 'twitter/EpiOAuth.php';
	require_once 'twitter/EpiTwitter.php';

	$Twitter = new EpiTwitter( $consumer_key, $consumer_secret );
	$Twitter->setToken( $token, $secret );

	$direct_message = $Twitter->post_direct_messagesNew( array( 'user' => $user, 'text' => $message ) );
	$tweet_info = $direct_message->responseText;

}

function isEmail( $email ) { // Email address verification, do not edit.

	return preg_match( "/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i", $email );

}
?>
