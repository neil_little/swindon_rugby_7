<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <h2>Registration Payment</h2>

    <p>Please fill in the form below to pay the access the registeration fee.</p>

    <input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="business" value="swindon7s@gmail.com">
    <input type="hidden" name="item_name" value="Swindon 7s Registration Fee">
    <input type="hidden" name="amount" value="75.00">
    <input type="hidden" name="no_shipping" value="0">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="currency_code" value="GBP">
    <input type="hidden" name="lc" value="AU">
    <input type="hidden" name="bn" value="PP-BuyNowBF">
    <input type="hidden" name="return" value="http://swindon7s.co.uk/payment-complete.php">

    <input type="submit" value="Pay Registration" id="paypalBtn" class="button">
</form>