<nav id="topNav" class="small-12 columns">
	<a href="/" id="navLogo"></a>

	<ul id="mainNav" class="navigation">
		<li><a href="/enter-a-team">Enter a team</a></li>
		<!-- <li><a href="/current-teams">Current teams</a></li> -->
		<li><a href="/sponsor">Sponsor</a></li>
		<li><a href="/gallery">Gallery</a></li>
		<li><a href="/news">news</a></li>
		<li><a href="/festival">festival</a></li>
		<li><a class="target-blank" href="//www.zazzle.co.uk/swindon7s">shop</a></li>
		<li><a href="/contact-us">Contact Us</a></li>
	</ul>
</nav>