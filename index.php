<!doctype html>
<!--[if lte IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/">
	<meta name="description" content="We are proud to have created a unique event that has made a significant impact on the world's sporting scene, with many different teams competing every year.">
	<title>Swindon 7's: Home</title>

	<meta property="og:url" content="http://swindon7s.co.uk/">
	<meta property="og:title" content="Swidon 7's: Home"/>
	<meta property="og:description" content="We are proud to have created a unique event that has made a significant impact on the world's sporting scene, with many different teams competing every year."/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body data-page="home">
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<?php require_once("parts/hero-slider.php"); ?>

			<section class="join-strip no-select small-12 colums">
				<?php require_once("parts/join-strip.php"); ?>
			</section>

			<?php require_once("parts/3-block-adv.php"); ?>
			<article class="sponsor-table">
				<section class="medium-12 large-4 columns">
					<a href="//www.silverbacksport.co.uk/">
						<div class="child-blk our-sponsors" style="background-color: rgb(35,31,32);border-top:0;border-left: 0">
							<img src="/images/sponsor-sliverback.png" alt="Silverback" class="sponsor-logo">
						</div>
					</a>
				</section>

				<section class="medium-12 large-4 columns">
					<a href="//www.greeneking.co.uk/">
						<div class="child-blk partner">
							<img src="/images/sponsor-greene-king.png" alt="Greene King" class="sponsor-logo">
						</div>
					</a>
				</section>

				<section class="medium-12 large-4 columns">
					<a href="//www.thekhyber.co.uk/">
						<div class="child-blk become-a-sponsor">
							<img src="/images/sponsor-kyber.png" alt="Kyber Indian Food" class="sponsor-logo">
						</div>
					</a>
				</section>
			</article>
		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>