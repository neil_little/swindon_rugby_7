<!doctype html>
<html lang="en" class="no-js">
<head>
		<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/register">
	<meta name="description" content="Registration is now closed for this year. Why not visit the games!">
	<title>Swindon 7's: Register</title>

	<meta property="og:url" content="http://swindon7s.co.uk/register">
	<meta property="og:title" content="Swidon 7's: Register"/>
	<meta property="og:description" content="Registration is now closed for this year. Why not visit the games!"/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="register small-12 columns">

			<div class="contact-form medium-12 columns">

				<h1>Registration</h1>

				<div id="message" class="regform"></div>

				<form method="post" action="/parts/register.php" name="contactform" id="contactform" class="registerpage">

					<section class="team-info">
						<h3>Team Info</h3>

						<input name="regTeamName" class="team-name" type="text" placeholder="Team Name">
						<input type="email" name="email" placeholder="PayPal Email">

						<textarea name="mainContact" type="text" placeholder="Main contact details"></textarea>

					</section>

					<section class="team-members">
						<h3>Team Players</h3>

						<p>Please enter the team names, so we have them on record. If you have less than the 16 players, type <code>--</code> to confirm "not used"!</p>
						
						<input type="text" name="playerName1" placeholder="1">
						<input type="text" name="playerName2" placeholder="2">
						<input type="text" name="playerName3" placeholder="3">
						<input type="text" name="playerName4" placeholder="4">
						<input type="text" name="playerName5" placeholder="5">
						<input type="text" name="playerName6" placeholder="6">
						<input type="text" name="playerName7" placeholder="7">
						<input type="text" name="playerName8" placeholder="8">
						<input type="text" name="playerName9" placeholder="9">
						<input type="text" name="playerName10" placeholder="10">
						<input type="text" name="playerName11" placeholder="11">
						<input type="text" name="playerName12" placeholder="12">
						<input type="text" name="playerName13" placeholder="13">
						<input type="text" name="playerName14" placeholder="14">
						<input type="text" name="playerName15" placeholder="15">
						<input type="text" name="playerName16" placeholder="16">
						<textarea name="teamContactInfo" type="text" placeholder="TEAM CONTACT INFO"></textarea>
					</section>

					<input type="submit" id="submit" class="button half" value="SUBMIT">
				</form>

			</div>

			<?php //require_once("parts/paypal-btn.php"); ?>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>