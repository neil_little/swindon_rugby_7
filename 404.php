<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php require_once("parts/meta.php"); ?>
	<meta name="description" content="Swindon 7's - Shop">
	<title>Swindon 7's: Shop</title>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="site-error small-12 columns" style="min-height:580px">

			<h1 class="error-code">404</h1>

			<h2 class="explain">dead link</h2>

			<h3 class="access-url">
				<a href="/">Take Me Home</a>
			</h3>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>