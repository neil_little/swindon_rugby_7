<!doctype html>
<!--[if lte IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/">
	<meta name="description" content="We are proud to have created a unique event that has made a significant impact on the world's sporting scene, with many different teams competing every year.">
	<title>Swindon 7's: Gallery</title>

	<meta property="og:url" content="http://swindon7s.co.uk/">
	<meta property="og:title" content="Swidon 7's: Gallery"/>
	<meta property="og:description" content="We are proud to have created a unique event that has made a significant impact on the world's sporting scene, with many different teams competing every year."/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body data-page="home">
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="small-12 columns">

			<div id="promoCanvas" class="contact-header">
				<section class="title-container">
					<h1 class="no-select">s7's <span class="blue-fnt">Gallery</span></h1>
				</section>

				<aside class="social-media">
					<a class="target-blank promoCanvas-twitter" href="//twitter.com/Swindon7s" target="_blank"></a><a class="target-blank promoCanvas-facebook" href="//facebook.com/Swindon7sFestival" target="_blank"></a>
				</aside>
			</div>

			<section id="gallery__wrapper" class="small-12 columns">
				<nav id="gallery__inner" class="row clearfix">
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="img-1" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/img-1.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="img-2" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/img-2.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="img-3" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/img-3.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="img-4" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/img-4.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="img-5" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/img-5.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="swindon7s-flyers" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/swindon7s-flyers.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="swindon7s-vip-area" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/swindon7s-vip-area.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10572221_663139293769854_6909961595922780247_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10572221_663139293769854_6909961595922780247_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10551474_663140347103082_8171807857162419012_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10551474_663140347103082_8171807857162419012_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10547744_662567020493748_6949347706142846987_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10547744_662567020493748_6949347706142846987_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10519195_663139147103202_4608957129848933653_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10519195_663139147103202_4608957129848933653_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10505124_663139197103197_596115769161029883_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10505124_663139197103197_596115769161029883_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10497874_663139603769823_604016010224587836_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10497874_663139603769823_604016010224587836_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10497154_663237703760013_2349202203799536084_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10497154_663237703760013_2349202203799536084_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10496050_662567397160377_4563041371652911043_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10496050_662567397160377_4563041371652911043_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10428351_662567180493732_7324945534161729447_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10428351_662567180493732_7324945534161729447_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="10387161_662566400493810_4918341330704715440_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/10387161_662566400493810_4918341330704715440_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="1548012_663140320436418_4747464842910614792_o" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/1548012_663140320436418_4747464842910614792_o.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="73529_10200444230492689_1238484451_n" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/73529_10200444230492689_1238484451_n.jpg" data-title="Swindon 7s Festival"></a>
					<a class="small-3 columns" data-lightbox="swindon7s" data-gallery-filename="969581_10200444231452713_2032063055_n" data-gallery-filetype="jpg" data-gallery-alt="Swindon 7s" href="../images/gallery/969581_10200444231452713_2032063055_n.jpg" data-title="Swindon 7s Festival"></a>
				</nav>
			</section>

			

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns"><?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime small-12 columns"><?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
</body>
</html>