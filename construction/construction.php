<!doctype html>
<html lang="en" class="no-js">
<head>
	<!-- DNS Prefetch -->
	<link rel="dns-prefetch" href="//ajax.google.com">
	<!-- Meta's -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=1200, user-scalable=yes">
	<!-- CSS -->
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,900,700' rel='stylesheet'>
	<link rel="stylesheet" href="../css/app.css" />
	<link rel="stylesheet" href="../css/rfc.css" />
	<!-- JS Libary (SCRIPTS @ BOTTOM) -->
	<script src="/bower_components/modernizr/modernizr.js"></script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-47579383-1', 'swindon7s.co.uk');
	  ga('send', 'pageview');

	</script>
	<link rel="canonical" href="http://swindon7s.co.uk/construction" />
	<meta name="description" content="Swindon 7s Festival is the sport and music festival that takes place every year on 19.07.14. Launched in 20012 the Festival has quickly become renowned as the Sporting Vfest, attracting hundreds of sports enthusiasts and festival goers.">
	<title>Swindon 7's: We won't be long!</title>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="site-error small-12 columns" style="min-height:580px">

			<h1 class="error-code">under construction</h1>

			<h3 class="access-url">we wont be long!</h3>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<script src="/bower_components/jquery/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				$('#mainNav').remove();
				$('.link-section a').attr("href", "/");
				$('.link-section').delay(1000).slideUp();
			});
		</script>
		<script src="/build/plugins.min.js"></script>
		<script src="/build/script.min.js"></script>
</body>
</html>