// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

/*
 *    PLUGIN FILE
 *
 *    All plugins to be added here to save bandwidth !
 *
*/

// http://stackoverflow.com/questions/14923301/uncaught-typeerror-cannot-read-property-msie-of-undefined-jquery-tools 
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


/*
 *    INCLUDE LIBS
 *
*/

// @codekit-append "libs/jquery.jigowatt.js"
// @codekit-append "libs/jquery-ui-1.10.4.custom.min.js"
// @codekit-append "libs/jquery.cycle2.min.js"
// @codekit-append "libs/jquery.placeholder.min.js"
// @codekit-append "libs/jquery.social.stream.1.5.4.min.js"
// @codekit-append "libs/jquery.social.stream.wall.1.3.js"
// @codekit-append "libs/jquery.lightbox.min.js"