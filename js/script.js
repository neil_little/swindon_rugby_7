/*global Modernizr:true, skrollr:true */
/*jshint -W030, unused:false, undef:false */

/**
 * MAIN SCRIPTS FILE
 */

/**
 * GLOBAL VARIABLES
 */

var targetBlank 		= $('.target-blank'),
	gallery__wrapper 	= $('#gallery__wrapper'),
	gallery__inner 		= $('#gallery__inner');

 /**
 * RE-USABLE FUNCTIONS
 */


 /**
 * MODULAR FUNCTIONS
 */

 /**
 * ALWAYS WATCHING
 */
$(window).on('hashchange', function processHashChange(e) {
	e.preventDefault();

	// FESTIVAL PAGE

		$('.festival-list').tabs({
			beforeActivate: function( event, ui ) {
				ui.newTab.index();
			}
		});
});

 /**
 * DOC READY
 */

(function ($) {
	$(document).ready(function(){

		// GLOBAL

			targetBlank.attr("target","_blank");

			// PLACEHOLDER POLLYFILL
			$('input, textarea').placeholder();

		// HOME PAGE

			$('#heroSlider').cycle({
				log:false
			});

		// NEWS SECTION

		$('#social-stream').dcSocialStream({
			feeds: {
				// twitter: {
				// 	id: 'Swindon7s',
				// 	url: 'http://swindon7s.co.uk/twitter.php',
				// 	thumb:true,
				// 	images:'medium',
				// 	out:'intro,thumb,text,share',
				// 	retweets:false,
				// 	replies:false
				// },
				facebook: {
					id: '128030474027923'
				}
			},
			rotate: {
				delay: 0
			},
			twitterId: 'Swindon7sFestival',
			control: false,
			remove: 'https://www.facebook.com/Swindon7sFestival/posts/263883703775932',
			filter: true,
			wall: true,
			max: 'limit',
			limit: 25,
			iconPath: '/images/dcsns-dark/',
			imagePath: '/images/dcsns-dark/'
		});

		// CONTACT SECTION

		// is the gallery wrapper present? Crack on with the code snippet below!s
		if ( gallery__wrapper.length ){

			/*
				This script:

					- Cycles through each a link under the inner gallery
					- Collect the data attr, then stores them in a variable
					- Pushes data through a template ($gallery__template)
						-- Replicated on every a link inside = gallery__inner (#gallery__inner)

					-- FUTURE ADDITIONS

						-- ERRORS OUT ON NO ATTR PRESENT
						-- $gallery__alt = FALL BACK
							-- String = {please add data-gallery-alt"...."}
						-- $gallery__filetype = FALL BACK
							-- Replace with string default = jpg

			*/
			$('a', gallery__inner).each(function(){
				var $el					= $(this),
					$gallery__filename	= $el.data('gallery-filename'),
					$gallery__filetype	= $el.data('gallery-filetype');
					$gallery__alt 		= $el.data('gallery-alt'),

					$gallery__template	= $('<img src="../images/gallery/fitted/' + $gallery__filename + '-fitted.' + $gallery__filetype + '" alt="' + $gallery__alt + '" class="gallery__pic">');

				$(this).append( $gallery__template );
			});

		}

	});
}(jQuery));