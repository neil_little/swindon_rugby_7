<!doctype html>
<html lang="en" class="no-js">
<head>
		<?php require_once("parts/meta.php"); ?>
	<link rel="canonical" href="http://swindon7s.co.uk/register">
	<meta name="description" content="">
	<title>Swindon 7's: Register</title>

	<meta property="og:url" content="http://swindon7s.co.uk/register">
	<meta property="og:title" content="Swidon 7's: Register"/>
	<meta property="og:description" content=""/>
	<?php require_once("parts/facebook-og-uni.php"); ?>
</head>
<body>
	<div class="row">
		<?php require_once("parts/top-nav.php"); ?>

		<section id="middleSection" class="site-error small-12 columns" style="min-height:580px">

			<h1 class="error-code">Registeration Is Now Closed</h1>

			<h2 class="explain">We have now closed registration, opening in the new year!</h2>

			<h3 class="access-url">
				<a href="/">Take Me Home</a>
			</h3>

		</section>

		<section id="footerSection" class="small-12 columns">

			<div class="row">
				<div class="link-section small-12 columns">
					<?php require_once("parts/footer-links.php"); ?>
				</div>

				<div class="sosuime dark small-12 columns">
					<?php require_once("parts/footer.php"); ?>
				</div>
			</div>
			
		</section>
	</div>
		<?php require_once("parts/body-js.php"); ?>
		<script>
			$(document).ready(function(){
				$('#mainNav').remove();
				$('.link-section a').attr("href", "/");
				$('.link-section').delay(1000).slideUp();
			});
		</script>
</body>
</html>